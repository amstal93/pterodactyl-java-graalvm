# Pterodactyl (Docker) build for GraalVM (Java)

## Preparation

Clone repository and edit Dockerfile to update FROM if needed (`openjdk:17-slim` to `openjdk:XX-slim` if not based on _Java 17_ but _Java XX_)

Edit in `build` file these variables:
- JAVA_VERSION_MAJOR
- JAVA_VERSION_MINOR
- GRAAL_VERSION_MAJOR
- GRAAL_BASE (if needed)

## Build image

Run `sudo chmod +x build && sudo bash ./build`

This gives to you `zogg/graalvm-{os}-{arch}:latest` and `zogg/graalvm-{os}-{arch}:{graalvm full version}` images to add on your Pterodactyl Minecraft servers to run you servers under GraalVM.

Enjoy :)

## Pterodactyl settings

You may use these startup flags in Pterodactyl:

```
-Dterminal.jline=false -Dterminal.ansi=true -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:InitiatingHeapOccupancyPercent=15 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true
```

## Pterodactyl run & logs

This should gave you these output in you sever's console :

```
container@pterodactyl~ java -version
openjdk version "17.0.2" 2022-01-18
OpenJDK Runtime Environment GraalVM CE 22.0.0.2 (build 17.0.2+8-jvmci-22.0-b05)
OpenJDK 64-Bit Server VM GraalVM CE 22.0.0.2 (build 17.0.2+8-jvmci-22.0-b05, mixed mode, sharing)
container@pterodactyl~ java -Xms128M -Xmx2048M -Dterminal.jline=false -Dterminal.ansi=true -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:InitiatingHeapOccupancyPercent=15 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true -jar fabric-server-launch.jar nogui
```

## Java Startup Flags

### Command line you should try

```
java -Xms128M -Xmx{{SERVER_MEMORY}}M -XX:+UnlockDiagnosticVMOptions -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true -XX:+EnableJVMCIProduct -XX:+EnableJVMCI -XX:+UseJVMCICompiler -XX:+EagerJVMCI -XX:+UseStringDeduplication -XX:+UseFastUnorderedTimeStamps -XX:+UseAES -XX:+UseAESIntrinsics -XX:AllocatePrefetchStyle=1 -XX:+UseLoopPredicate -XX:+RangeCheckElimination -XX:+EliminateLocks -XX:+DoEscapeAnalysis -XX:+UseCodeCacheFlushing -XX:+UseFastJNIAccessors -XX:+OptimizeStringConcat -XX:+UseCompressedOops -XX:+UseThreadPriorities -XX:+OmitStackTraceInFastThrow -XX:+TrustFinalNonStaticFields -XX:+UseInlineCaches -XX:+RewriteBytecodes -XX:+RewriteFrequentPairs -XX:+UseNUMA -XX:-DontCompileHugeMethods -XX:+UseCMoveUnconditionally -XX:+UseFPUForSpilling -XX:+UseVectorCmov -XX:+UseXMMForArrayCopy -Dfile.encoding=UTF-8 -Djdk.nio.maxCachedBufferSize=262144 -Dgraal.CompilerConfiguration=community -Dgraal.SpeculativeGuardMovement=true --add-modules jdk.incubator.vector  -jar {{SERVER_JARFILE}} nogui
```

### Aikar flags

```
-XX:+UseG1GC \
-XX:+ParallelRefProcEnabled \
-XX:MaxGCPauseMillis=200 \
-XX:+UnlockExperimentalVMOptions \
-XX:+DisableExplicitGC \
-XX:+AlwaysPreTouch \
-XX:G1NewSizePercent=30 \
-XX:G1MaxNewSizePercent=40 \
-XX:G1HeapRegionSize=8M \
-XX:G1ReservePercent=20 \
-XX:G1HeapWastePercent=5 \
-XX:G1MixedGCCountTarget=4 \
-XX:InitiatingHeapOccupancyPercent=15 \
-XX:G1MixedGCLiveThresholdPercent=90 \
-XX:G1RSetUpdatingPauseTimePercent=5 \
-XX:SurvivorRatio=32 \
-XX:+PerfDisableSharedMem \
-XX:MaxTenuringThreshold=1 \
-Dusing.aikars.flags=https://mcflags.emc.gs \
-Daikars.new.flags=true \
```

### Etil Minecraft Flags

```
{Aikar flags, then}
-XX:-UseBiasedLocking \
-XX:+EnableJVMCIProduct \
-XX:+EnableJVMCI \
-XX:+UseJVMCICompiler \
-XX:+EagerJVMCI \
-XX:UseAVX=2 \
-XX:+UseStringDeduplication \
-XX:+UseFastUnorderedTimeStamps \
-XX:+UseAES \
-XX:+UseAESIntrinsics \
-XX:UseSSE=4 \
-XX:AllocatePrefetchStyle=1 \
-XX:+UseLoopPredicate \
-XX:+RangeCheckElimination \
-XX:+EliminateLocks \
-XX:+DoEscapeAnalysis \
-XX:+UseCodeCacheFlushing \
-XX:+UseFastJNIAccessors \
-XX:+OptimizeStringConcat \
-XX:+UseCompressedOops \
-XX:+UseThreadPriorities \
-XX:+OmitStackTraceInFastThrow \
-XX:+TrustFinalNonStaticFields \
-XX:ThreadPriorityPolicy=1 \
-XX:+UseInlineCaches \
-XX:+RewriteBytecodes \
-XX:+RewriteFrequentPairs \
-XX:+UseNUMA \
-XX:-DontCompileHugeMethods \
-XX:+UseCMoveUnconditionally \
-XX:+UseFPUForSpilling \
-XX:+UseNewLongLShift \
-XX:+UseVectorCmov \
-XX:+UseXMMForArrayCopy \
-XX:+UseXmmI2D \
-XX:+UseXmmI2F \
-XX:+UseXmmLoadAndClearUpper \
-XX:+UseXmmRegToRegMoveAll \
-Dfile.encoding=UTF-8 \
-Djdk.nio.maxCachedBufferSize=262144 \
-Dgraal.TuneInlinerExploration=1 \
-Dgraal.CompilerConfiguration=enterprise \
-Dgraal.UsePriorityInlining=true \
-Dgraal.Vectorization=true \
-Dgraal.OptDuplication=true \
-Dgraal.DetectInvertedLoopsAsCounted=true \
-Dgraal.LoopInversion=true \
-Dgraal.VectorizeHashes=true \
-Dgraal.EnterprisePartialUnroll=true \
-Dgraal.VectorizeSIMD=true \
-Dgraal.StripMineNonCountedLoops=true \
-Dgraal.SpeculativeGuardMovement=true \
-Dgraal.InfeasiblePathCorrelation=true \
--add-modules jdk.incubator.vector \
```

## Sources

- https://github.com/pterodactyl/yolks
- https://github.com/Software-Noob/pterodactyl-images
- https://quay.io/repository/pterodactyl/core/manifest/sha256:c0139c52f8ae3581e64c5d598b1d4da7328599a7abb41b76d66487900a3396f7
- https://github.com/graalvm/graalvm-ce-builds/releases/tag/vm-22.0.0.2
- https://github.com/lustefaniak/docker-graalvm
- https://hub.docker.com/_/openjdk
- https://aikar.co/mcflags.html
- https://github.com/etil2jz/etil-minecraft-flags
