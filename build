#!/bin/bash
# ----------------------------------
# Pterodactyl Core Dockerfile Builder
# Environment: Java /  GraalVM
# Minimum Panel Version: 1.7.0
# ----------------------------------

clear
cd "$(dirname "$0")" || exit 1

# setup: Java & GraalVM versions
JAVA_VERSION_MAJOR=17
JAVA_VERSION_MINOR=.1.0
GRAAL_VERSION_MAJOR=22
GRAAL_HOME=/opt/java/graalvm

# patch: docker-java-home
echo "#/bin/sh" > docker-java-home
echo "echo ${GRAAL_HOME}" >> docker-java-home

# setup: Image names & Tags
JAVA_VERSION=${JAVA_VERSION_MAJOR}${JAVA_VERSION_MINOR}
GRAAL_JAVA=${JAVA_VERSION_MAJOR}
GRAAL_VERSION=${GRAAL_VERSION_MAJOR}${JAVA_VERSION_MINOR}
GRAAL_OS=linux
GRAAL_ARCH=aarch64

IMAGE_BASE=zogg/graalvm-${GRAAL_OS}-${GRAAL_ARCH}
IMAGE_VERSION=${GRAAL_VERSION}
IMAGE_NAME_LATEST=${IMAGE_BASE}:latest
IMAGE_NAME_VERSION=${IMAGE_BASE}:${IMAGE_VERSION}

rm -f graalvm.tgz
graal=graalvm-ee-java${JAVA_VERSION_MAJOR}-${GRAAL_OS}-${GRAAL_ARCH}-${GRAAL_VERSION}.tar.gz
ln -s $graal graalvm.tgz

rm -f native-image.jar
native=native-image-installable-svm-svmee-java${JAVA_VERSION_MAJOR}-${GRAAL_OS}-${GRAAL_ARCH}-${GRAAL_VERSION}.jar
ln -s $native native-image.jar

# Build Image
#    --no-cache \

docker build \
    --build-arg GRAAL_HOME=${GRAAL_HOME} \
    --build-arg JAVA_VERSION=${JAVA_VERSION} \
    --build-arg GRAAL_JAVA=${GRAAL_JAVA} \
    --build-arg GRAAL_VERSION=${GRAAL_VERSION} \
    --build-arg GRAAL_OS=${GRAAL_OS} \
    --build-arg GRAAL_ARCH=${GRAAL_ARCH} \
    -t "${IMAGE_NAME_LATEST}" \
    -t "${IMAGE_NAME_VERSION}" \
    .

exit 0
